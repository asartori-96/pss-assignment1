# Use Python 3.7
FROM python:3.7

ENV PYTHONUNBUFFERED 1

# Make and set working directory
RUN apt-get update && apt-get install -y && apt-get install python3-pip -y
WORKDIR /yourlist-app
COPY . /yourlist-app

# Install all packages specified in requirements.txt
RUN pip3 install -r requirements.txt

# Open to the outside port 8000
#EXPOSE 8000