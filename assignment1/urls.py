from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('yourlist/', include('yourlist.urls')),
    path('admin/', admin.site.urls),
    path('', include('django_prometheus.urls')),
]