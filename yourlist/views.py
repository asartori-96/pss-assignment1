from django.shortcuts import render
from django.http import Http404, HttpResponseRedirect

from .forms import UserForm, InterestForm
# from django.http import HttpResponse

from .models import Person, Interest
from django.views import generic
from .forms import UserForm

def index(request):
    users_list = Person.objects.all()
    context = {
        'users_list': users_list,
    }
    return render(request, 'yourlist/index.html', context)

def userView(request, person_id):
    try:
        user = Person.objects.get(pk=person_id)
        interests_list = Interest.objects.filter(person_id=person_id)
        context = {
            'interests_list': interests_list,
            'user': user,
        }
    except Person.DoesNotExist:
        raise Http404("Ops! It seems the person you are looking for does not exist in our system")
    return render(request, 'yourlist/userview.html', context)

def userCreate(request):
    form = UserForm(request.POST or None)

    form_valid = form.is_valid()

    if form_valid:
        form.save()

    context = {
        'form': form,
        'form_valid': form_valid
    }
    
    return render(request, 'yourlist/usercreate.html', context)

def userDelete(request, person_id):
    user = Person.objects.filter(pk=person_id)
    user.delete()

    context = {
        'user': user,
    }

    return render(request, 'yourlist/userdelete.html', context)

def interestCreate(request, person_id):

    user = Person.objects.get(pk=person_id)

    form = InterestForm(request.POST or None, initial={'person': person_id})

    form_valid = form.is_valid()

    if form_valid:
        form.save()
    
    context = {
        'user': user,
        'form': form,
        'form_valid': form_valid
    }

    return render(request, 'yourlist/interestcreate.html', context)

def interestRemove(request, interest_id):
    interest = Interest.objects.get(pk=interest_id)
    user = Person.objects.get(pk=interest.person_id)
    interest.delete()

    context = {
        'user': user,
        'interest': interest,
        'user': user,
    }

    return render(request, 'yourlist/interestremove.html', context)

# Create your views here.
