from django import forms
from .models import Person, Interest

class UserForm(forms.ModelForm):
    class Meta:
        model = Person
        fields = ["name", "surname", "age"]

class InterestForm(forms.ModelForm):
    class Meta:
        model = Interest
        fields = ["person","description", "category"]