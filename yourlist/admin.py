from django.contrib import admin

from .models import Person, Interest

admin.site.register(Person)
admin.site.register(Interest)