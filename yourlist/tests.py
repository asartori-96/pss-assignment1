from django.test import TestCase
from yourlist.models import Person, Interest

# Test on Person instances
class PersonTestCase(TestCase):
    def setUp(self):
        Person.objects.create(name="Mario", surname="Rossi", age="23")
    
    def testPersAttribute(self):
        user = Person.objects.get(name="Mario")
        self.assertEqual(user.age, 23)
        self.assertEqual(user.surname, "Rossi")

# Test on Interest instances
class InterestTestCase(TestCase):
    def setUp(self):
        user = Person.objects.create(name="Anna", surname="Bianchi", age="34")
        Interest.objects.create(person_id=user.id, description="Tennis", category="Sport")
    
    def testInterAttribute(self):
        interest = Interest.objects.get(description="Tennis")
        user = Person.objects.get(id=interest.person_id)
        self.assertEqual(interest.category, "Sport")
        self.assertEqual(user.name, "Anna")

# Test on 404 error
class Error404TestCase(TestCase):
    def setUp(self):
        Person.objects.create(id=999, name="Filippo", surname="Neri", age="18")

    def test404Error(self):
        response = self.client.get('/yourlist/user/1/')
        self.assertEqual(response.status_code, 404)