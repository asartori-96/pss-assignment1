from django.apps import AppConfig


class YourlistConfig(AppConfig):
    name = 'yourlist'
