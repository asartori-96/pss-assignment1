from django.urls import path
from . import views

app_name = 'yourlist'
urlpatterns = [
    path('', views.index, name='index'),
    path('user/<int:person_id>/', views.userView, name='userView'),
    path('user/create/', views.userCreate, name='userCreate'),
    path('user/<int:person_id>/delete/', views.userDelete, name='userDelete'),
    path('user/<int:person_id>/newinterest/', views.interestCreate, name='interestCreate'),
    path('user/interest/<int:interest_id>/delete/', views.interestRemove, name='interestRemove')
]