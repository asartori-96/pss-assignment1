from django.db import models

CATEGORY_OPTIONS = (('sport','Sport'), ('politics','Politics'), ('music','Music'), ('technology','Technology'),('miscellanues','Miscellanues'),)

class Person(models.Model):
    name = models.CharField(max_length=30)
    surname = models.CharField(max_length=30)
    age = models.IntegerField(default=0)

    def __str__(self):
        return self.surname

class Interest(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    description = models.CharField(max_length=100)
    category = models.CharField(max_length=255, choices=CATEGORY_OPTIONS)

    def __str__(self):
        return self.description

# Create your models here.
