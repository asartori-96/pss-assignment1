# Simple script to stop and remove active containers

if [[ -z $(docker ps -a -q) ]]; then
   echo "No active containers found. Nothing to do."
else
   echo "Stopping containers..."
   docker container stop $(docker ps -a -q)
   echo "Removing containers..."
   docker container rm $(docker ps -a -q)
   echo "Done."
fi